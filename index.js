
'use strict'

let agent = {}
  , args = agent.args = require('argparse')
  , compression = agent.compression = require('compression')
  , crypto = agent.crypto = require('crypto')
  , cp = agent.cp = require('child_process')
  , express = agent.express = require('express')
  , extend = agent.extend = require('extend')
  , favicon = agent.favicon = require('serve-favicon')
  , fs = agent.fs = require('fs-extra')
  , http = agent.http = require('http')
  , https = agent.https = require('https')
  , mysql = agent.mysql = require('mysql')
  , socketio = agent.socketio = require('socket.io')
  , morgan = agent.morgan = require('morgan')
  , path = agent.path = require('path')
  , url = agent.url = require('url')
  , Q = require('q')
  , shortId = require('shortid')
  , ipaddress = process.env.OPENSHIFT_NODEJS_IP || process.env.IP || `127.0.0.1`
  , port = process.env.OPENSHIFT_NODEJS_PORT || process.env.PORT || 3000
  , status = agent.status = {
      online: false
  }
  , config = agent.config = fs.readJSONSync(path.resolve('./config.json'))
  , dbConnect  = agent.dbConnect = ()=>{
    process.stdout.write(`Connecting to database at : ${ config.host + ":" + config.port }\n`);
    return new Promise(resolve=>{
      var connection = agent.connection = mysql.createConnection({
        host     : config.host,
        port     : config.port,
        user     : config.user,
        password : config.password,
        database: config.database
      })
      resolve(connection)
    })
  }


dbConnect().then(connection=>{
  connection.connect(err=>{
    if (err) {
      // process.stdout.write(`Error connecting to database described by : ${ connection}\n` + err.stack)
      console.log(`Error connecting to database described by : `+ connection + `\n` + err.stack)
      return 
    }
    process.stdout.write(`Connected as ID: ${connection.threadId}\n`)
    let app = agent.app = express()
    let server = agent.server = http.createServer(app)
    let io = agent.io = socketio.listen(server)
    io.on('connection',socket=>{
      agent.events = require(path.resolve(__dirname, 'events','opd'))(agent,socket)      
      agent.events = require(path.resolve(__dirname, 'events','ipd'))(agent,socket)      
    })
    // app.use(express.static(path.resolve(__dirname, 'www'), { maxAge: 86400000 }))
    app.engine('html', require('ejs').renderFile)
    app.set('views', path.resolve(__dirname , 'www'))
    app.use(favicon(path.resolve(__dirname, 'www','assets','icon','favicon.png')))
    app.use(compression({ threshold: 1024 }))
    app.use(morgan('dev'))
    let params = agent.params = require(path.resolve(__dirname, 'params','patientid'))(agent)
    agent.general_routes = require(path.resolve(__dirname, 'routes','general'))(agent)
    agent.opd_routes = require(path.resolve(__dirname, 'routes','opd'))(agent)
    agent.ipd_routes = require(path.resolve(__dirname, 'routes','ipd'))(agent)
    function onerr(err) {
        console.log(err)
        function inc(p) {
          p++
          server = http.Server(app).listen(p)
        }
        if (err.code == 'EADDRINUSE') {
          inc(port)
        }
      }
      server.listen(port, function() {
        var addr = server.address()
        console.log("api running on %s:%s", addr.address, addr.port)
      }).on('error', onerr)
    })
}).catch(err=>{process.stdout.write(`Err: ${JSON.stringify(err)}`)})