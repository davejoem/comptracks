module.exports = (agent)=>{
  agent.app.get("/api/opd", (req,res)=>{
    agent.connection.query("SELECT *", (error, results, fields)=>{
      console.log(error, results, fields)
      if (error) throw error;
      res.json(JSON.stringify(results));
    })
  })
  agent.app.get("/api/opd/dec/diagnosis", (req,res)=>{
    // let startDate = req.query
    agent.connection.query("SELECT Outpatients_Admission.AdmissionID AS Adm,Outpatients_Admission.Diagnosis As FullNames,Outpatients_Admission.AgeToday,PatientProcedures.AdmissionID AS PatAdm,PatientProcedures.CategoryName,PatientProcedures.ServiceName,PatientProcedures.OtherDiagnosis,PatientProcedures.TreatmentDate FROM Outpatients_Admission LEFT JOIN PatientProcedures ON Outpatients_Admission.AdmissionID = PatientProcedures.AdmissionID where TreatmentDate >= 2016-12-01 and TreatmentDate <= 2016-12-31 AND Type='DIAGNOSIS' AND Class='OPD' ORDER BY PatientProcedures.ID", (error, results, fields)=>{
      console.log(error,results)
      if (error) throw error;
      res.json(JSON.stringify(results))
    })
  })
  agent.app.get("/api/patient/:patientid", (req,res)=>{
    res.json(JSON.stringify(req.user))
  })
  agent.app.get("/api/patients", (req,res)=>{
    agent.connection.query(`SELECT ID,treatmentdate,fullnames,servicename FROM patientprocedures`, (err, results, fields)=>{
      if (err) {
        return res.json(err);
      } 
      else if (results) {
        res.json(results)
      }
      else res.json("No data to display")
    })
  })
  agent.app.get("/api/opd/diagnosis", (req,res)=>{
    agent.connection.query(`SELECT Outpatients_Admission.AdmissionID AS Adm,Outpatients_Admission.Diagnosis As FullNames,Outpatients_Admission.AgeToday,PatientProcedures.AdmissionID AS PatAdm,PatientProcedures.CategoryName,PatientProcedures.ServiceName,PatientProcedures.OtherDiagnosis,PatientProcedures.TreatmentDate FROM Outpatients_Admission LEFT JOIN PatientProcedures ON Outpatients_Admission.AdmissionID = PatientProcedures.AdmissionID where TreatmentDate >= "2016-12-01" and TreatmentDate <= "2016-12-31" AND Type='DIAGNOSIS' AND Class='OPD' ORDER BY PatientProcedures.ID`, (err, results, fields)=>{
      if (err) {
        return res.json(err);
      } 
      else if (results) {
        res.json(results)
      }
      else res.json("No data to display")
    })
  })
  agent.app.get("/api/patients/opd", (req,res)=>{
    agent.connection.query(`SELECT ID,treatmentdate,fullnames,servicename FROM patientprocedures WHERE class="opd"`, (err, results, fields)=>{
      if (err) {
        return res.json(err);
      } 
      else if (results) {
        res.json(results)
      }
      else res.json("No data to display")
    })
  })
}