module.exports = function(agent){
  agent.app.get('/docs', (req,res,next)=>{
    res.render(agent.path.resolve(__dirname, '..', 'www','docs.html'))
  })
  agent.app.get('/api', (req,res,next)=>{
    res.redirect("/docs")
  })
  agent.app.get('/', (req,res,next)=>{
    console.log(__dirname)
    res.render(agent.path.resolve(__dirname, '..', 'www','index.html'))
  })
}