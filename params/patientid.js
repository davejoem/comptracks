module.exports = (agent)=>{
  agent.app.param('patientid', function(req, res, next, id) {
    agent.connection.query(`SELECT * FROM patientprocedures WHERE patientid="${id}"`
    , (err, results, fields)=>{
      if (err) {
        next(err);
      } else if (results) {
        req.user = results;
        next();
      } else {
        next(new Error('failed to load user'));
      }
    })
  })
  agent.app.param('startdate', function(req, res, next, startdate) {
    req.startdate = startdate
    next();    
  })
  agent.app.param('enddate', function(req, res, next, enddate) {
    req.enddate = enddate
    next();    
  })
}
